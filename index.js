console.log("Hello world");

// [SECTION] JASON Object
/*
	- JSON standards for JavaScript Object Notation
	- JSON is also used in other programming languages
	- Core JavaScript has a built-in JSON objects that contains methods for passing JSON objects
	- JSON is used for serializing different types into bytes
*/

// JSON Object
/*
	JSON also uses the "key/value pairs" just like the object properties in JavaScript
	- Key/Property names should be enclosed with double quotes

	Syntax:
	{
		"propertyA" : "valueA",
		"propertyB" : "valueB"
	}
*/
/*
{
	"city" : "Quezon City",
	"province" : "Metro Manila",
	"country" : "Philippines"
}
*/

// JSON Arrays
// Arrays in JSON are almost same as arrays in JavaScript
// Arrays in JSON Object
/*
	"cities" : [
		{
			"city" : "Quezon City",
			"province" : "Metro Manila",
			"country" : "Philippines"
		},
		{
			"city" : "Manila City",
			"province" : "Metro Manila"
			"country" : "Philippines"
		},
		{
			"city" : "Makita City",
			"province" : "Metro Manila",
			"country" : "Philippines"
		}
	]
*/

// [SECTION] JSON Methods
// The "JSON Object" contains methods for parsin and converting data in stringified JSON
// JSON data is sent or received in text-only(String) format

// Converting Data Into Stringified JSON


let batchesArray = [
	{
		batchName: 230,
		schedule: "Part Time"		
	},
	{
		batchName: 240,
		schedule: "Fulltime"
	}

];

console.log(batchesArray);

console.log("Result from stringify method: ");
/* Syntax : JSON.stringify(arrays/objects); */
console.log(JSON.stringify(batchesArray));


let data = JSON.stringify(
	{
		name : "John",
		age : 31,
		address : {
			city : "Manila",
			country: "Philippines"
		}
	}
);

console.log(data);

// User Details
/*let firstName = prompt ("Enter your first name: ");
let lastName = prompt("Enter your last name: ");
let email = prompt("Enter your email");
let password = prompt("Enter your password");

let info = JSON.stringify(
{
	firstName : firstName,
	lastName : lastName,
	email : email,
	password : password
});

console.log(info);*/

// Converting Stringified JSON into JavaScript Objects:

let batchesJSON = `[
	{
		"batchName" : 230,
		"schedule" : "Part Time"		
	},
	{
		"batchName" : 240,
		"schedule" : "Fulltime"
	}
]`;

console.log("batchesJSON content: ");
console.log(batchesJSON);

console.log("Result from parse method: ");
let parseBatches = JSON.parse(batchesJSON);
console.log(parseBatches);
console.log(parseBatches[0].batchName);



let stringifiedObject = `
	{
		"name" : "John",
		"age" : 31,
		"address" : {
			"city" : "Manila",
			"country" : "Philippines"
		}
	}
`

console.log(stringifiedObject);

let parsedData = JSON.parse(stringifiedObject); // console.log(JSON.parse(stringifiedObject));
console.log(parsedData);